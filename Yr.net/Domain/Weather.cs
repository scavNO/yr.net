﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yr.net.domain
{
    class Weather
    {
        private int id { get; set; }
        private String country { get; set; }
        private String location { get; set; }
        private DateTime fromDate { get; set; }
        private DateTime toDate { get; set; }
        private String weatherName { get; set; }
        private String weatherSymbol { get; set; }
        private int temperature { get; set; }
        private String windDirectionCode { get; set; }
        private String windDirectionText { get; set; }
        private double windSpeed { get; set; }
        private String windTypeName { get; set; }

        public override String ToString()
        {
            return "Weather{" +
                    "id=" + id +
                    ", country='" + country + '\'' +
                    ", fromDate=" + fromDate +
                    ", toDate=" + toDate +
                    ", weatherName='" + weatherName + '\'' +
                    ", symbol=" + weatherSymbol +
                    ", temperature=" + temperature +
                    ", windDirectionCode='" + windDirectionCode + '\'' +
                    ", windDirectionText='" + windDirectionText + '\'' +
                    ", windSpeed=" + windSpeed +
                    ", windTypeName='" + windTypeName + '\'' +
                    '}';
        }
    }
}
