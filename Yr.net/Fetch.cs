﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yr.net.Location;
using Yr.net.domain;

namespace Yr.net
{
    public class Fetch
    {
        /// <summary>
        /// Fetch the complete list of location resources.
        /// </summary>
        /// <returns></returns>
        public List<LocationNorway> Get()
        {
            return new WeatherLocationNorway().GetLocationsNorway();
        }

        public LocationNorway Norway(String county, String localArea)
        {
            List<LocationNorway> locationNorwayList = new WeatherLocationNorway().GetLocationsNorway();
            return new FindLocationNorway(locationNorwayList).Query(county, localArea);
        }
    }
}
