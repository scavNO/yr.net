﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yr.net.Location
{
    class FindLocationNorway
    {
        private Dictionary<LocationKey, LocationNorway> locationQueryList = 
            new Dictionary<LocationKey, LocationNorway>();

        public FindLocationNorway(List<LocationNorway> locationNorwayLost)
        {
            foreach (LocationNorway norwayLocation in locationNorwayLost)
            {
                this.locationQueryList.Add(new LocationKey(norwayLocation.county, norwayLocation.localArea), norwayLocation);
            }
        }

        public LocationNorway Query(String county, String localArea)
        {
            LocationKey locationKey = new LocationKey(county, localArea);
            return locationQueryList[locationKey];
        }
    }
}
