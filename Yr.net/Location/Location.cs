﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yr.net.Location
{
    public class Location
    {
        public String country { set; get; }
        public String location { set; get; }
        public String url { set; get; }

        public Location() { }

        public Location(String country, String location, String url)
        {
            this.country = country;
            this.location = location;
            this.url = url;
        }
    }
}
