﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yr.net.Location
{
    public class LocationKey
    {
        private String queryTermOne { get; set; }
        private String queryTermTwo { get; set; }

        public LocationKey(String queryTermOne, String queryTermTwo)
        {
            this.queryTermOne = queryTermOne;
            this.queryTermTwo = queryTermTwo;
        }

        public class EqualityComparer : IEqualityComparer<LocationKey>
        {
            public bool Equals(LocationKey x, LocationKey y)
            {
                return x.queryTermOne == y.queryTermOne && x.queryTermTwo == y.queryTermTwo;
            }

            public int GetHashCode(LocationKey x)
            {
                return x.queryTermOne.GetHashCode() ^ x.queryTermTwo.GetHashCode();
            }
        }
    }
}
