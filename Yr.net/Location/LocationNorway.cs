﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yr.net.Location
{
    public class LocationNorway : Location 
    {
        public String county { set; get; }
        public String localArea { set; get; }

        public LocationNorway(String country, String location, String url, String county, String localArea)
            : base(country, location, url)
        {
            this.county = county;
            this.localArea = localArea;
        }

        public LocationNorway()
        {
            // TODO: Complete member initialization
        }

        public override string ToString()
        {
            return "Country: " + country + "\n" +
                    "Location: " + location + "\n" +
                    "Url: " + url + "\n" +
                    "County: " + county + "\n" +
                    "LocalArea: " + localArea + "\n";
        }
    }
}
