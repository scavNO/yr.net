﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Yr.net.Location;

namespace Yr.net
{
    public class WeatherLocationNorway
    {
        private readonly String DocumentLocation = "http://fil.nrk.no/yr/viktigestader/noreg.txt";
        private List<String> documentLines = new List<String>();
        private List<LocationNorway> locationNorwayList = new List<LocationNorway>();

        public WeatherLocationNorway()
        {
            CreateParsableList();
        }

        public List<LocationNorway> GetLocationsNorway()
        {
            foreach (String line in documentLines)
            {
                String[] result = line.Split('\t');

                locationNorwayList.Add(new LocationNorway
                    {
                        country = "Norway",
                        location = result[1],
                        url = result[12],
                        county = result[7],
                        localArea = result[1]
                    }
                );                
            }

            return locationNorwayList;
        }

        /// <summary>
        /// Fetches the file containing all the locations and add it to a list
        /// separated by new line.
        /// </summary>
        /// <returns></returns>
        private void CreateParsableList()
        {
            using (StreamReader reader = new StreamReader
                (WebRequest.Create(DocumentLocation).GetResponse().GetResponseStream()))
            {
                String line = String.Empty;
                while((line = reader.ReadLine()) != null)
                {
                    documentLines.Add(line);
                }
            }
        }
    }
}
