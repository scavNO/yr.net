﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yr.net;
using Yr.net.Location;

namespace Yr.netLauncher
{
    class Program
    {
        static void Main(string[] args)
        {

            LocationNorway locationNorway = new Fetch().Norway("Østfold", "Brekke");

            Console.WriteLine(locationNorway.ToString());
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();                
        }
    }
}
