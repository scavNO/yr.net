﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Yr.net.Location;

namespace Yr.netTest
{
    [TestClass]
    public class TestFindLocationNorway
    {

        public Dictionary<LocationKey, LocationNorway> locationQueryList =
            new Dictionary<LocationKey, LocationNorway>(new LocationKey.EqualityComparer());

        /// <summary>
        /// Set up the initial test data.
        /// This mock is useful and isolates the test.
        /// </summary>
        [TestInitialize]
        public void init()
        {
            locationQueryList.Add(
                new LocationKey("Bergen", "Hordaland"),
                new LocationNorway
                {
                    country = "Norway",
                    location = "Bergen",
                    url = "http://",
                    county = "Hordaland",
                    localArea = "Bergen"
                }
            );

            locationQueryList.Add(
                new LocationKey("Sogndal", "Sogn og Fjordane"),
                new LocationNorway
                {
                    country = "Norway",
                    location = "Sogndal",
                    url = "http://",
                    county = "Sogn og Fjordane",
                    localArea = "Sogndal"
                }
            );

            locationQueryList.Add(
                new LocationKey("Trondheim", "Sør Trøndelag"),
                new LocationNorway
                {
                    country = "Norway",
                    location = "Trondheim",
                    url = "http://",
                    county = "Sør Trøndelag",
                    localArea = "Trondheim"
                }
            );

        }

        /// <summary>
        /// Test that the location query is able to fetch a location based
        /// on the LocationKey class.
        /// </summary>
        [TestMethod]
        public void Test_Find_Location_Query()
        {

            Assert.IsNotNull(locationQueryList);

            var compareLocationNorway = new LocationNorway
            {
                country = "Norway",
                location = "Trondheim",
                url = "http://",
                county = "Sør Trøndelag",
                localArea = "Trondheim"
            };           

            LocationNorway locationNorway = locationQueryList[new LocationKey("Trondheim", "Sør Trøndelag")];
            Assert.IsNotNull(locationNorway);
            Assert.AreNotEqual(compareLocationNorway, locationNorway); 
        }
    }
}
