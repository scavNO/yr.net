﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yr.net;
using Yr.net.Location;
using System.Collections.Generic;
//using Yr.net.Domain;

namespace Yr.netTest
{
    [TestClass]
    public class TestWeatherLocationNorway
    {
        /// <summary>
        /// Test that weather locations are being fetched from the yr.no txt file.
        /// </summary>
        [TestMethod]
        public void Test_Get_Weather_Locations_Norway()
        {
            List<LocationNorway> weatherLocationNorwayList = new WeatherLocationNorway().GetLocationsNorway();
            Assert.IsNotNull(weatherLocationNorwayList);
        }

    }
}
